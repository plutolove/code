import java.util.*;
import java.io.*;
public class cf462B {
    public static void main(String args[]) throws Exception{
//        System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int k=in.nextInt();
        int cont[]=new int[26];
        String str=in.next();
        Arrays.fill(cont,0);
        for(int i=0;i<n;i++) {
            cont[str.charAt(i)-'A']++;
        }
        Arrays.sort(cont);
        long res=0;
        for(int i=cont.length-1;i>=0;i--) {
            if(cont[i]<k) {
                res+=(long)cont[i]*(long)cont[i];
                k-=cont[i];
            }else {
                res+=(long)k*(long)k;
                break;
            }
        }
        System.out.println(res);
    }
}
