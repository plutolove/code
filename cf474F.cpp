#include<cstdlib>
#include<cctype>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<vector>
#include<string>
#include<iostream>
#include<sstream>
#include<map>
#include<set>
#include<queue>
#include<stack>
#include<fstream>
#include<numeric>
#include<iomanip>
#include<bitset>
#include<list>
#include<stdexcept>
#include<functional>
#include<utility>
#include<ctime>
using namespace std;
const int N=100005;
const int inf=1e9+1e4;
struct node {
    int l,r;
    int minv,cont;
    int gc;
    int mid() {
        return (l+r)>>1;
    }
    void set(int l,int r) {
        this->l=l;
        this->r=r;
    }
};
node t[N<<2];
int Minv,Gc,sum;
int gcd (int a,int b) {
    return b? gcd(b,a%b):a;
}
void push_up(int rt) {
    int ls=rt<<1;
    int rs=rt<<1|1;
    if(t[ls].minv<t[rs].minv) {
        t[rt].minv=t[ls].minv;
        t[rt].cont=t[ls].cont;
    }else if(t[ls].minv>t[rs].minv) {
        t[rt].minv=t[rs].minv;
        t[rt].cont=t[rs].cont;
    }else if(t[ls].minv==t[rs].minv) {
        t[rt].minv=t[ls].minv;
        t[rt].cont=t[ls].cont+t[rs].cont;
    }
    t[rt].gc=gcd(t[ls].gc,t[rs].gc);
}
void build(int l,int r,int rt) {
    t[rt].set(l,r);
    if(l==r) {
        cin>>t[rt].minv;
        t[rt].gc=t[rt].minv;
        t[rt].cont=1;
        return;
    }
    int m=t[rt].mid();
    build(l,m,rt<<1);
    build(m+1,r,rt<<1|1);
    push_up(rt);
}
void query(int rt,int L,int R) {
    if(t[rt].l>=L&&R>=t[rt].r) {
        if(t[rt].minv<Minv) {
            Minv=t[rt].minv;
            sum=t[rt].cont;
        }else if(t[rt].minv==Minv) {
            sum+=t[rt].cont;
        }
        Gc=gcd(Gc,t[rt].gc);
        return;
    }
    int m=t[rt].mid();
    if(m>=L) query(rt<<1,L,R);
    if(m<R) query(rt<<1|1,L,R);
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n;
    cin>>n;
    build(1,n,1);
    int m;
    cin>>m;
    while(m--) {
        int l,r;
        Minv=inf;
        Gc=0;
        sum=0;
        cin>>l>>r;
        query(1,l,r);
        if(Minv==Gc) {
            cout<<r-l+1-sum<<endl;
        }else cout<<r-l+1<<endl;
    }
    return 0;
}
