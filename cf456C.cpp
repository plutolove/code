#include<iostream>
#include<cstring>
#include<string>
#include<cstdio>
using namespace std;
int n,m;
string str;
bool solve(int id) {
    for(char i=str[id]+1;i<m+'a';i++) {
        if(str[id-1]!=i&&str[id-2]!=i) {
            str[id]=i;
            return true;
        }
    }
    return false;
}
int main() {
    #ifndef ONLINE_JUDGE
    //freopen("in.txt","r",stdin);
    #endif
    while(cin>>n>>m>>str) {
        int index=-1;
        for(int i=n-1;i>=0;i--) {
            if(solve(i)) {
                index=i;
                break;
            }
        }
        if(index==-1) {
            cout<<"NO"<<endl;
            continue;
        }
        for(int i=index+1;i<n;i++) {
            str[i]='a'-1;
            solve(i);
        }
        cout<<str<<endl;
    }
    return 0;
}
