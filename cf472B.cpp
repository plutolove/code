#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
using namespace std;
const int N=2005;
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n,k;
    priority_queue<int> q;
    cin>>n>>k;
    int f;
    for(int i=0;i<n;i++) {
        cin>>f;
        q.push(f);
    }
    int ans=0;
    int cont=0;
    while(q.size()>0) {
        int t=q.top();
        if(cont==0) {
            ans+=(t-1)*2;
        }
        q.pop();
        cont++;
        if(cont==k) {
            cont=0;
        }
    }
    cout<<ans<<endl;
    return 0;
}
