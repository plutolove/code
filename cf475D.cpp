#include<iostream>
#include<cstring>
#include<cstdio>
#include<map>
using namespace std;
typedef long long LL;
const int N=100005;
LL a[N];
LL gcd(LL x,LL y) {
    if(y==0) return x;
    return gcd(y,x%y);
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    map<LL,LL> dp[2],res;
    int n;
    cin>>n;
    for(int i=0;i<n;i++) {
        cin>>a[i];
    }
    dp[0][a[0]]++;
    res[a[0]]++;
    for(int i=1;i<n;i++) {
        int now=i&1;
        int pre=1-now;
        dp[now].clear();
        int num=a[i];
        dp[now][num]++;
        res[num]++;
        for(map<LL,LL>::iterator it=dp[pre].begin();it!=dp[pre].end();it++) {
            int g=gcd(num,it->first);
            dp[now][g]+=it->second;
            res[g]+=it->second;
        }
    }
    int m,q;
    cin>>m;
    while(m--) {
        cin>>q;
        cout<<res[q]<<endl;
    }
    return 0;
}
