#include<iostream>
#include<cstdio>
#include<string>
using namespace std;
typedef pair<string,string> pss;
const int N=100005;
pss str[N];
int id[N];
string now[N];
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n;
    cin>>n;
    string s1,s2;
    for(int i=1;i<=n;i++) {
        cin>>s1>>s2;
        str[i]=make_pair(s1,s2);
    }
    now[0]="A";
    bool f=0;
    int i;
    for(int i=1;i<=n;i++) {
        cin>>id[i];
    }
    for(int i=1;i<=n;i++) {
        int index=id[i];
        if(str[index].first>now[i-1]&&str[index].second>now[i-1]) {
            now[i]=min(str[index].first,str[index].second);
        }else if(str[index].first>now[i-1]) {
            now[i]=str[index].first;
        }else if(str[index].second>now[i-1]) {
            now[i]=str[index].second;
        }else {
            f=1;
            break;
        }
    }
    if(f) cout<<"NO"<<endl;
    else cout<<"YES"<<endl;
    return 0;
}
