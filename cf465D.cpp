#include<iostream>
#include<cstring>
#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long LL;
const int N=10;
LL p[N][3],d[N];
LL dis(int i,int j) {
    LL x=p[i][0]-p[j][0],y=p[i][1]-p[j][1],z=p[i][2]-p[j][2];
    return x*x+y*y+z*z;
}
bool judge() {
    for(int i=0;i<8;i++) {
        for(int j=0;j<8;j++) {
            if(i==j) continue;
            else if(j<i) {
                d[j]=dis(i,j);
            }else d[j-1]=dis(i,j);
        }
        sort(d,d+7);
        LL t1=d[0],t2=2*d[0],t3=3*d[0];
        if(t1==0||t1!=d[1]||t1!=d[2]||t2!=d[3]||t2!=d[4]||t2!=d[5]||t3!=d[6])return false;
    }
    return true;
}
bool has(int id) {
    if(id==8) {
        return judge();
    }
    sort(p[id],p[id]+3);
    do {
        if(has(id+1)) return true;
    }while(next_permutation(p[id],p[id]+3));
    return false;
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    for(int i=0;i<8;i++) {
        cin>>p[i][0]>>p[i][1]>>p[i][2];
    }
    if(has(0)) {
        cout<<"YES"<<endl;
        for(int i=0;i<8;i++) {
            cout<<p[i][0]<<' '<<p[i][1]<<' '<<p[i][2]<<endl;
        }
    }else {
        cout<<"NO"<<endl;
    }
    return 0;
}
