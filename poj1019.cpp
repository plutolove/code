#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
using namespace std;
typedef long long LL;
const int N=31270;
LL sum[N],len[N];
void init() {
    len[0]=0;
    sum[0]=0;
    for(int i=1;i<N;i++) {
        len[i]=len[i-1]+(LL)log10((double)i)+1;
        sum[i]=sum[i-1]+len[i];
    }
}
void solve(int n) {
    int tmp=1;
    for(int i=1;i<n;i++) tmp=i;
    int lens=0;
    int pos=n-sum[tmp-1];
    int id=1;
    while(lens<pos) {
        lens+=(int)log10((double)id)+1;
        id++;
    }
    int res=((id-1)/(int)pow(10.0,lens-pos))%10;
    cout<<res<<endl;
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int t;
    init();
    cin>>t;
    while(t--) {
        int n;
        cin>>n;
        solve(n);
    }
    return 0;
}
