#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <queue>
#include <map>
#include <set>
using namespace std;
#define N 65
#define M 65*65
typedef __int64 LL;
LL U[M],D[M],L[M],R[M],C[M],X[M];
LL H[N],S[N];
LL size,n,k;
LL city[N][2];
LL airport[N][2];
LL dis[N][N];
LL MMM;
LL mx[N][N];
bool vis[N];
LL h()
{
    LL i,j,c;
    memset(vis,0,sizeof(vis));
    LL ans = 0;
    for(c = R[0] ; c ; c = R[c])
    {
        if(!vis[c])
        {
            ans++;
            for(i = D[c]; i != c; i = D[i])
                for(j = R[i]; j != i; j = R[j])
                    vis[C[j]] = true;
        }
    }
    return ans;
}

void remove(LL c)
{
    for(LL i = D[c]; i != c; i = D[i])
        R[L[i]] = R[i], L[R[i]] = L[i];
}

void resume(LL c)
{
    for(LL i = D[c]; i != c; i = D[i])
        R[L[i]] = L[R[i]] = i;
}
bool Dance(LL dep)
{
    if(R[0] == 0)
        return true;
    if(dep + h() > k)
        return false;

    LL i,j,tmp,c;

    for(tmp=100000,i=R[0]; i; i=R[i])
        if(S[i]<tmp)tmp=S[c=i];

    for(i=D[c]; i!=c; i=D[i])
    {
        remove(i);
        for(j=R[i]; j!=i; j=R[j])remove(j);
        if(Dance(dep+1)) return 1;
        for(j=L[i]; j!=i; j=L[j])resume(j);
        resume(i);
    }

    return 0;
}
void Link(LL r,LL c)
{
    ++S[C[size]=c];
    D[size]=D[c];
    U[D[c]]=size;
    U[size]=c;
    D[c]=size;
    if(H[r]<0)H[r]=L[size]=R[size]=size;
    else
    {
        R[size]=R[H[r]];
        L[R[H[r]]]=size;
        L[size]=H[r];
        R[H[r]]=size;
    }
    X[size++]=r;
}

bool build(LL mid){
    memset(mx,0,sizeof(mx));
    for(LL i=1;i<=n;i++){
        LL flag=0;
        for(LL j=1;j<=n;j++){
            if(dis[j][i] - mid < 1e-8){
                mx[j][i] = 1;
                flag=1;
            }
        }
        if(!flag)  {
            // puts("..");
            return 0;
        }
    }

    for(LL i=0; i<=n; ++i)
    {
        S[i]=0;
        D[i]=U[i]=i;
        L[i+1]=i;
        R[i]=i+1;
    }
    R[n]=0;
    size=n+1;
    for(LL i=1; i<=n; ++i)
        H[i]=-1;

    for(LL i=1;i<=n;i++){
        for(LL j=1;j<=n;j++){
            if(mx[i][j])
                Link(i,j);
        }
    }
    return 1;
}
bool test(LL mid)
{
    if(build(mid))  {
        // puts("..");
        return Dance(0);
    }
        return false;
}

LL solve()
{
    LL low = 0;
    LL high = MMM;

    LL ans = 0;
    while(low <= high)
    {
        LL mid = (low+high)/2;
        if(test(mid))
            high = mid - 1, ans = mid;
        else
            low = mid + 1;
    }
    return ans;
}
LL Dis(LL i, LL j)
{
    return abs(airport[i][0] - city[j][0]) + abs(airport[i][1] - city[j][1]);
}

void Init()
{
    LL i,j;
    for(i = 1; i <= n; i++)
        for(j = 1; j <= n; j++)
            dis[i][j] = Dis(i,j), MMM = max(MMM, dis[i][j]);
}
int main()
{
#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
#endif
    LL i,t;
    scanf("%I64d",&t);
    LL ti = 0;
    while(t--)
    {
        MMM = 0;
        scanf("%I64d%I64d",&n,&k);
        for(i = 1; i <= n; i++)
            scanf("%I64d%I64d",&city[i][0],&city[i][1]);
        for(i = 1; i <= n; i++)
            airport[i][0] = city[i][0], airport[i][1] = city[i][1]
        Init();
        printf("Case #%I64d: %I64d\n",++ti, solve());
    }
    return 0;
}
