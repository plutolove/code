#include<iostream>
#include<cstring>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
const int N=100005;
const double pi=acos(-1.0);
struct node {
    double x,h;
    bool operator<(const node& a)const {
        return x<a.x;
    }
};
struct qu {
    double x;
    int id;
    bool operator<(const qu& qq)const {
        return x<qq.x;
    }
};
int n,m;
qu q[N];
node p[N];
node st[N];
double ans[N];
bool judge(node a,node b,node c) {
    return (a.h-c.h)*(c.x-b.x)>=(b.h-c.h)*(c.x-a.x);
}
double calc(node a,node b) {
    double tmp=a.x-b.x;
    if(tmp<0) tmp*=-1.0;
    return atan(tmp/a.h);
}
bool judge1(node a,node b,node c) {
    return (a.h-c.h)*(b.x-c.x)>=(b.h-c.h)*(a.x-c.x);
}
void solve() {
    int top=0;
    int index=0;
    for(int i=0;i<m;i++) {
        while(index<n&&q[i].x>p[index].x) {
            while(top&&st[top-1].h<p[index].h) top--;
            while(top>1&&judge(st[top-2],st[top-1],p[index])) top--;
            st[top++]=p[index];
            index++;
        }
        node tmp;
        tmp.h=0;
        tmp.x=q[i].x;
        while(top>1&&judge(st[top-2],st[top-1],tmp)) top--;
        ans[q[i].id]+=calc(st[top-1],tmp);
    }
    top=0;
    index=n-1;
    for(int i=m-1;i>=0;i--) {
        while(index>=0&&q[i].x<p[index].x) {
            while(top&&st[top-1].h<p[index].h) top--;
            while(top>1&&judge1(st[top-2],st[top-1],p[index])) top--;
            st[top++]=p[index];
            index--;
        }
        node tmp;
        tmp.h=0;
        tmp.x=q[i].x;
        while(top>1&&judge1(st[top-2],st[top-1],tmp)) top--;
        ans[q[i].id]+=calc(st[top-1],tmp);
    }
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int t;
    scanf("%d",&t);
    for(int kc=1;kc<=t;kc++) {

        scanf("%d",&n);
        for(int i=0;i<n;i++) {
            scanf("%lf %lf",&p[i].x,&p[i].h);
        }
        sort(p,p+n);

        scanf("%d",&m);
        for(int i=0;i<m;i++) {
            scanf("%lf",&q[i].x);
            q[i].id=i;
        }
        sort(q,q+m);

        memset(ans,0,sizeof(ans));
        solve();
        printf("Case #%d:\n",kc);
        for(int i=0;i<m;i++) {
            printf("%.10lf\n", ans[i]*180.0/pi);
        }
    }
    return 0;
}
