import java.util.*;
import java.io.*;
public class round263D {
    public static void main(String args[]) throws Exception{
        cf462D solve=new cf462D();
        solve.input();
        solve.dfs(-1,0);
        System.out.println(solve.dp[0][1]);
    }
}
class cf462D {
    final static int N=100005;
    final static long mod=1000000007;
    ArrayList<ArrayList<Integer>> g;
    public long dp[][]=new long[N][2];
    int color[]=new int [N];
    int next[]=new int [N];
    int n;
    public void input() throws Exception{
        //System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        n=in.nextInt();
        g=new ArrayList<ArrayList<Integer>>();
        for(int i=0;i<n-1;i++) {
            next[i]=in.nextInt();
        }
        for(int i=0;i<n;i++) {
            color[i]=in.nextInt();
            g.add(new ArrayList<Integer>());
            Arrays.fill(dp[i],0);
        }
        for(int i=0;i<n-1;i++) {
            g.get(i+1).add(next[i]);
            g.get(next[i]).add(i+1);
        }
    }
    public void dfs(int f,int u) {
        dp[u][color[u]]=1;
        dp[u][color[u]^1]=0;
        ArrayList<Integer> tmp=g.get(u);
        for(Integer v:tmp) {
            if(v==f) continue;
            dfs(u,v);
            dp[u][1]=((((dp[v][0]+dp[v][1])%mod)*dp[u][1])%mod+dp[u][0]*dp[v][1])%mod;
            dp[u][0]=((dp[v][0]+dp[v][1])%mod)*dp[u][0]%mod;
        }
    }
}
