import java.util.*;
import java.io.*;
public class cf462A {
    public static void main(String args[]) throws Exception{
//        System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        String str[]=new String[n];
        for(int i=0;i<n;i++) {
            str[i]=in.next();
        }
        boolean f=false;
        for(int i=0;i<n;i++) {
            for(int j=0;j<str[i].length();j++) {
                int cont=0;
                if(i-1>=0&&str[i-1].charAt(j)=='o') cont++;
                if(i+1<n&&str[i+1].charAt(j)=='o') cont++;
                if(j-1>=0&&str[i].charAt(j-1)=='o') cont++;
                if(j+1<n&&str[i].charAt(j+1)=='o') cont++;
                if(cont%2==1) {
                    f=true;
                    break;
                }
            }
            if(f) break;
        }
        if(!f) System.out.println("YES");
        else System.out.println("NO");
    }
}
