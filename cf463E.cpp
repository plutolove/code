#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<vector>
using namespace std;
const int N=100005;
int val[N],f[N];
vector<int> g[N];
int gcd(int a,int b) {
    return b==0? a:gcd(b,a%b);
}
void dfs(int p,int u) {
    f[u]=p;
    for(int i=0;i<g[u].size();i++) {
        int v=g[u][i];
        if(v==p) continue;
        dfs(u,v);
    }
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n,q,x,y,z;
    memset(f,0,sizeof(f));
    scanf("%d%d",&n,&q);
    for(int i=1;i<=n;i++) {
        scanf("%d",&val[i]);
    }
    for(int i=1;i<n;i++) {
        scanf("%d%d",&x,&y);
        g[x].push_back(y);
        g[y].push_back(x);
    }
    dfs(0,1);
    while(q--) {
        scanf("%d",&x);
        if(x==1) {
            scanf("%d",&y);
            int i;
            for(i=f[y];i!=0;i=f[i]) {
                if(gcd(val[i],val[y])>=2) {
                    break;
                }
            }
            if(i==0) puts("-1");
            else cout<<i<<endl;
        }else {
            scanf("%d%d",&y,&z);
            val[y]=z;
        }
    }
    return 0;
}
