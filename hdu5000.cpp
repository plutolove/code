#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=2005;
const int mod=1e9+7;
typedef long long LL;
LL dp[2][N*N];
int a[N];
void init(int id,int n) {
    for(int i=0;i<=n;i++) {
        dp[id][i]=0;
    }
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int t;
    scanf("%d",&t);
    while(t--) {
        int n;
        scanf("%d",&n);
        int sum=0;
        for(int i=1;i<=n;i++) {
            scanf("%d",&a[i]);
            sum+=a[i];
        }
        memset(dp,0,sizeof(dp));
        dp[0][0]=1;
        int now,last;
        for(int i=1;i<=n;i++) {
            for(int j=0;j<=a[i];j++) {
                for(int k=sum;k>=j;k--) {
                    now=i&1;
                    last=now^1;
                    dp[now][k]+=dp[last][k-j];
                    dp[now][k]%=mod;
                    //cout<<dp[now][k]<<endl;
                }
            }
            init(last,sum);
        }
        cout<<dp[n&1][sum/2]<<endl;
    }
    return 0;
}
