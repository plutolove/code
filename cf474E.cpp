#include<cstdlib>
#include<cctype>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<vector>
#include<string>
#include<iostream>
#include<sstream>
#include<map>
#include<set>
#include<queue>
#include<stack>
#include<fstream>
#include<numeric>
#include<iomanip>
#include<bitset>
#include<list>
#include<stdexcept>
#include<functional>
#include<utility>
#include<ctime>
using namespace std;
typedef long long LL;
const int N=100005;
int dp[N],path[N],ah[N];
LL h[N],hh[N];
struct node {
    int tmax,id;
    node(){}
    node(int x,int y) {
        tmax=x;
        id=y;
    }
};
node t[N<<2];
int ansmax,pre;
void query(int l,int r,int rt,int L,int R) {
    if(l>=L&&R>=r) {
        if(t[rt].tmax>ansmax) {
            ansmax=t[rt].tmax;
            pre=t[rt].id;
        }
        return;
    }
    int m=(l+r)>>1;
    if(m>=L) query(l,m,rt<<1,L,R);
    if(m<R) query(m+1,r,rt<<1|1,L,R);
}
void push_up(int rt) {
    t[rt].tmax=max(t[rt<<1].tmax,t[rt<<1|1].tmax);
    if(t[rt].tmax==t[rt<<1].tmax) t[rt].id=t[rt<<1].id;
    else t[rt].id=t[rt<<1|1].id;
}
void update(int l,int r,int rt,int a,int b) {
    if(l==r) {
        t[rt].tmax=b;
        return;
    }
    int m=(l+r)>>1;
    if(m>=a) {
        update(l,m,rt<<1,a,b);
    }else update(m+1,r,rt<<1|1,a,b);
    push_up(rt);
}
void build(int l,int r,int rt) {
    t[rt].tmax=0;
    if(l==r) {
        t[rt].id=l;
        return;
    }
    int m=(l+r)>>1;
    build(l,m,rt<<1);
    build(m+1,r,rt<<1|1);
}

void Print(int p){
    if(path[p] == 0){
        printf("%d",p);
        return;
    }
    if(path[p]) Print(path[p]);
    printf(" %d",p);
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n;
    LL d;
    cin>>n>>d;
    for(int i=1;i<=n;i++) {
        cin>>h[i];
        hh[i]=h[i];
    }
    sort(hh+1,hh+n+1);
    int sz=unique(hh+1,hh+1+n)-hh-1;
    build(1,sz,1);
    memset(path,0,sizeof(path));
    for(int i=1;i<=n;i++) {
        int l=upper_bound(hh+1,hh+sz+1,h[i]-d)-hh;
        int r=lower_bound(hh+1,hh+sz+1,h[i]+d)-hh;
        ansmax=-1;
        if(r<=sz) query(1,sz,1,r,sz);
        if(l-1>=1) query(1,sz,1,1,l-1);
        int pos=lower_bound(hh+1,hh+sz+1,h[i])-hh;
        update(1,sz,1,pos,ansmax+1);
        path[i]=ah[pre];
        ah[pos]=i;
    }
    cout<<t[1].tmax<<endl;
    Print(ah[t[1].id]);
    return 0;
}
