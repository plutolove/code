/*
题意：
    H1 = A
    Hi = (Hi−1 + Hi+1)/2 − 1, for all 1 < i < N
    HN = B
    Hi ≥ 0, for all 1 ≤ i ≤ N
    求最小的Hn.
分析：二分 H2 的值判断是否合法.
 */
import java.util.*;
import java.io.*;
public class ural1066 {
    public static void main(String args[]) throws Exception {
        Solve tmp=new Solve();
        tmp.solve();
    }
}
class Solve {
    double a;
    int n;
    double res;
    boolean cheak(double m) {
        double h0=a;
        double h1=m;
        double h2=0;
        for(int i=0;i<n-2;i++) {
            h2=2*h1-h0+2.0;
            if(h2<0) return false;
            h0=h1;
            h1=h2;
        }
        res=h2;
        return true;
    }
    void solve() throws Exception {
        System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        n=in.nextInt();
        a=in.nextDouble();
        double l=0;
        double r=a;
        double ans=0;
        for(int i=0;i<100;i++) {
            double m=(l+r)/2.0;
            if(cheak(m)) {
                r=m;
                ans=res;
            }else {
                l=m;
            }
        }
        System.out.println(ans);
    }
}
