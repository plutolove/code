#include<cstdlib>
#include<cctype>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<vector>
#include<string>
#include<iostream>
#include<sstream>
#include<map>
#include<set>
#include<queue>
#include<stack>
#include<fstream>
#include<numeric>
#include<iomanip>
#include<bitset>
#include<list>
#include<stdexcept>
#include<functional>
#include<utility>
#include<ctime>
using namespace std;

#define PB push_back
#define MP make_pair

#define REP(i,n) for(i=0;i<(n);++i)
#define FOR(i,l,h) for(i=(l);i<=(h);++i)
#define FORD(i,h,l) for(i=(h);i>=(l);--i)

typedef vector<int> VI;
typedef vector<string> VS;
typedef vector<double> VD;
typedef long long LL;
typedef pair<int,int> PII;
const int N=200001;
const int M=51;
const int MM=401;
bitset<N> dp[M],path[M][M];
PII v[MM];
VI ans;
int n,m;
void init() {
    for(int i=0;i<M;i++) {
        dp[i].reset();
        for(int j=0;j<M;j++) {
            path[i][j].reset();
        }
    }
}
bool ff=0;
void print(int i,int x,int y) {
    if(ff) return;
    if(x==0&&y==0) {
        printf("%d",ans[0]);
        for(int i=1;i<ans.size();i++) {
            printf(" %d",ans[i]);
        }
        puts("");
        ff=1;
        return;
    }
    int cur=i/10;
    if(i%10) cur++;
    if(path[cur][x][y]==0) return;

    if(x>=v[i].first&&y>=v[i].second) {
        ans.push_back(i);
        print(i-1,x-v[i].first,y-v[i].second);
        ans.pop_back();
    }

    print(i-1,x,y);
}


void solve() {
    scanf("%d%d",&n,&m);
    init();
    for(int i=1;i<=n;i++) {
        scanf("%d%d",&v[i].first,&v[i].second);
    }
    dp[0][0]=1;
    int cont=0;
    for(int i=1;i<=n;i++) {
        for(int j=M-1;j>=v[i].first;j--) {
            dp[j]|=(dp[j-v[i].first]<<v[i].second);
        }
        if(i==n||i%10==0) {
            cont++;
            for(int j=M-1;j>0;j--) {
                path[cont][j]=dp[j];
            }
        }
    }
    int x,y;
    while(m--) {
        scanf("%d%d",&x,&y);
        if(dp[x][y]) {
            ans.clear();
            ff=0;
            print(n,x,y);
        }else {
            puts("No solution!");
        }
    }
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int t;
    scanf("%d",&t);
    while(t--) {
        solve();
    }
    return 0;
}
