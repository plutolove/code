#include<iostream>
#include<cstring>
#include<cstdio>
#include<vector>
using namespace std;
#define lson l,m,rt<<1
#define rson m+1,r,rt<<1|1
const int N=50010;
const int inf=1<<30;
struct node {
    int u,v;
    int cost;
};
node p[N*2];
int num[N],siz[N],top[N],son[N];
int dep[N],tid[N],fa[N];
int tree[N<<2];
int id;
int n;
int head[N],to[N*2],next[2*N],w[2*N],edg;
void init() {
    memset(head,-1,sizeof(head));
    memset(son,-1,sizeof(son));
    id=0;
    edg=0;
}
void add(int u,int v,int c)
{
    to[edg]=v,w[edg]=c,next[edg]=head[u],head[u]=edg++;
    to[edg]=u,w[edg]=c,next[edg]=head[v],head[v]=edg++;
}
void dfs1(int u,int f,int d) {
    dep[u]=d;
    fa[u]=f;
    siz[u]=1;
    for(int i=head[u];i!=-1;i=next[i]) {
        int v=to[i];
        if(v==f) continue;
        dfs1(v,u,d+1);
        siz[u]+=siz[v];
        if(son[u]==-1||siz[v]>siz[son[u]]) {
            son[u]=v;
        }
    }
}
void dfs2(int u,int tp) {
    top[u]=tp;
    tid[u]=++id;
    if(son[u]==-1) return;
    dfs2(son[u],tp);
    for(int i=head[u];i!=-1;i=next[i]) {
        int v=to[i];
        if(v!=son[u]&&v!=fa[u]) {
            dfs2(v,v);
        }
    }
}
void push_up(int rt) {
    tree[rt]=max(tree[rt<<1],tree[rt<<1|1]);
}
void build(int l,int r,int rt) {
    if(l==r) {
        tree[rt]=num[l];
        return;
    }
    int m=(l+r)>>1;
    build(lson);
    build(rson);
    push_up(rt);
}
void update(int l,int r,int rt,int pos,int val) {
    if(l==r) {
        tree[rt]=val;
        return;
    }
    int m=(l+r)>>1;
    if(pos<=m) update(lson,pos,val);
    else update(rson,pos,val);
    push_up(rt);
}
int query(int l,int r,int rt,int L,int R) {
    if(L<=l&&R>=r) {
        return tree[rt];
    }
    int m=(l+r)>>1;
    int res=-inf;
    if(L<=m) res=max(res,query(lson,L,R));
    if(R>m) res=max(res,query(rson,L,R));
    return res;
}
void change(int x,int val) {
    if(dep[p[x].u]>dep[p[x].v]) {
        update(2,n,1,tid[p[x].u],val);
    }else {
        update(2,n,1,tid[p[x].v],val);
    }
}
int search(int x,int y) {
    int ans=-inf;
    while(top[x]!=top[y]) {
        if(dep[top[x]]<dep[top[y]]) swap(x,y);
        ans=max(ans,query(2,n,1,tid[top[x]],tid[x]));
        x=fa[top[x]];
    }
    if(dep[x]>dep[y]) swap(x,y);
    if(x!=y) ans=max(ans,query(2,n,1,tid[x]+1,tid[y]));
    return ans;
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int t;
    scanf("%d",&t);
    while(t--) {
        init();
        scanf("%d",&n);
        int a,b,c;
        for(int i=1;i<n;i++) {
            scanf("%d%d%d",&a,&b,&c);
            p[i].u=a;
            p[i].v=b;
            p[i].cost=c;
            add(a,b,c);
        }
        dfs1(1,1,1);
        dfs2(1,1);
        for(int i=1;i<n;i++) {
            if(dep[p[i].u]>dep[p[i].v]) {
                num[tid[p[i].u]]=p[i].cost;
            }else {
                num[tid[p[i].v]]=p[i].cost;
            }
        }
        build(2,n,1);
        char op[20];
        while(true) {
            scanf("%s",op);
            if(op[0]=='D') break;
            int a,b;
            scanf("%d%d",&a,&b);
            if(op[0]=='Q') {
                printf("%d\n",search(a,b));
            }else change(a,b);
        }

    }
    return 0;
}
