#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=30;
int r[N][N];
int color[N];
int n;
bool hehe(int x) {
    for(int i=0;i<n;i++) {
        if(r[i][x]==1&&color[i]==color[x]) return false;
    }
    return true;
}
int dfs(int i,int co) {
    if(i==n) return n;
    for(color[i]=1;color[i]<=co;color[i]++) {
        if(!hehe(i)) {
            continue;
        }else {
            return dfs(i+1,co);
        }
    }
    return 0;
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    while(scanf("%d",&n)&&n) {
        getchar();
        memset(r,0,sizeof(r));
        memset(color,0,sizeof(color));
        char str[N];
        for(int i=0;i<n;i++) {
            scanf("%s",str);
            int len=strlen(str);
            for(int j=2;j<len;j++) {
                r[i][str[j]-'A']=1;
                r[str[j]-'A'][i]=1;
            }
        }
        int ans=0;
        for(int i=1;i<=4;i++) {
            if(dfs(0,i)) {
                ans=i;
                break;
            }
        }

        if(ans>1)
            printf("%d channels needed.\n",ans);
        else
            printf("1 channel needed.\n");
    }
    return 0;
}
