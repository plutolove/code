#include<iostream>
#include<cstring>
#include<map>
#include<cstdio>
using namespace std;
const int N=100005;
int f[N];
int x[N];
int find(int x) {
    if(x!=f[x]) return f[x]=find(f[x]);
    return f[x];
}
void unions(int x,int y) {
    int px=find(x);
    int py=find(y);
    if(px==py) return;
    f[px]=py;
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n,a,b;
    map<int,int> id;
    cin>>n>>a>>b;
    for(int i=1;i<=n;i++) {
        cin>>x[i];
        id[x[i]]=i;
    }
    for(int i=0;i<=n+2;i++) {
        f[i]=i;
    }
    for(int i=1;i<=n;i++) {

        if(id[a-x[i]]) {
            unions(i,id[a-x[i]]);
        }else unions(i,n+2);

        if(id[b-x[i]]) {
            unions(i,id[b-x[i]]);
        }else unions(i,n+1);

    }
    if(find(n+2)==find(n+1)) puts("NO");
    else {
        puts("YES");
        for(int i=1;i<=n;i++) {
            if(find(i)==find(n+2)) cout<<"1 ";
            else cout<<"0 ";
        }
        cout<<endl;
    }
    return 0;
}
