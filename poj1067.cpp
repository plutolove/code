#include<cstdio>
#include<cstring>
#include<cmath>
#include<iostream>
using namespace std;
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int a,b;
    while(cin>>a>>b) {
        if(a>b) swap(a,b);
        int k=b-a;
        int tmp=floor(k*(1.0+sqrt(5.0))/2.0);
        if(tmp==a) cout<<0<<endl;
        else cout<<1<<endl;
    }
    return 0;
}
