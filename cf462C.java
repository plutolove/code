import java.util.*;
import java.io.*;
public class cf462C {
    public static void main(String args[]) throws Exception{
        //System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        PriorityQueue<Long> q=new PriorityQueue<Long>();
        for(int i=0;i<n;i++) {
            long x=in.nextInt();
            q.add(-x);
        }
        long ans=0;
        while(q.size()>1) {
            long a=q.poll();
            long b=q.poll();
            q.add(a+b);
            ans+=(a+b);
        }
        ans+=q.poll();
        System.out.println(-ans);
    }
}
