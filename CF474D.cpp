#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
typedef long long LL;
const int N=100005;
const LL mod=1e9+7;
LL dp[N],sum[N];
void init(int t) {
    memset(dp,0,sizeof(dp));
    sum[0]=0;
    dp[0]=1;
    for(int i=1;i<N;i++) {
        dp[i]+=dp[i-1];
        dp[i]%=mod;
        if(i-t>=0) dp[i]+=dp[i-t];
        dp[i]%=mod;
        sum[i]=sum[i-1]+dp[i];
        sum[i]%=mod;
    }
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n,t;
    cin>>n>>t;
    init(t);
    while(n--) {
        int a,b;
        cin>>a>>b;
        LL ans=sum[b]-sum[a-1];
        ans=(ans+mod)%mod;
        cout<<ans<<endl;
    }
    return 0;
}
