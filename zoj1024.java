import java.util.*;
import java.io.*;
public class zoj1024 {
    public static void main(String args[]) throws Exception{
        Calendar_Game tmp=new Calendar_Game();
        tmp.solve();
    }
}
class Calendar_Game {
    final static int Y=2100;
    final static int M=20;
    final static int D=40;
    final static int[] month={0,31,28,31,30,31,30,31,31,30,31,30,31};
    int dp[][][]=new int[Y][M][D];
    void init() {
        for(int i=0;i<Y;i++) {
            for(int j=0;j<M;j++) {
                Arrays.fill(dp[i][j],-1);
            }
        }
    }
    boolean isleap(int y) {
        if(y%400==0||(y%100!=0&&y%4==0)) return true;
        return false;
    }
    boolean isok(int y,int m,int d) {
        if(isleap(y)&&m==2) {
            if(d<=29) return true;
            return false;
        }
        if(d<=month[m]) return true;
        return false;
    }
    int win(int y,int m,int d) {
        if(dp[y][m][d]!=-1) return dp[y][m][d];
        if(y==2001&&m==11&&d==4) {
            return dp[y][m][d]=0;
        }
        if(y>2001) {
            return dp[y][m][d]=1;
        }else if(y==2001&&m>11) {
            return dp[y][m][d]=1;
        }else if(y==2001&&m==11&&d>4) {
            return dp[y][m][d]=1;
        }
        int ny=y;
        int nm=m;
        int nd=d;

        if(isleap(y)&&m==2) {
            nd++;
            if(nd==30) {
                nd=1;
                nm=3;
            }
        }else {
            nd++;
            if(nd>month[nm]) {
                nd=1;
                nm++;
                if(nm>12) {
                    ny++;
                    nm=1;
                }
            }
        }
        if(win(ny,nm,nd)==0) return dp[y][m][d]=1;

        ny=y;
        nm=m;
        nd=d;
        nm++;
        if(nm>12) {
            ny++;
            nm=1;
        }
        if(isok(ny,nm,nd)&&win(ny,nm,nd)==0) return dp[y][m][d]=1;
        return dp[y][m][d]=0;
    }
    void solve() throws Exception{
        System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        init();
        int t=in.nextInt();
        for(int i=0;i<t;i++) {
            int y=in.nextInt();
            int m=in.nextInt();
            int d=in.nextInt();
            if(win(y,m,d)==1) System.out.println("YES");
            else System.out.println("NO");
        }
    }
}
