#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=2005;
const int inf=1e9+1e5;
int d[N][N];
int pre[N];
int minc[N];
int n;
bool vis[N];
LL dep[N];
vector<int> g[N];
void prim() {
    for(int i=1;i<=n;i++) {
        minc[i]=inf;
        vis[i]=0;
    }
    minc[1]=0;
    while(true) {
        int v=-1;
        for(int i=1;i<=n;i++) {
            if(!vis[i]&&(v==-1||minc[i]<minc[v])) {
                v=i;
            }
        }
        if(v==-1) break;
        vis[v]=1;
        for(int i=1;i<=n;i++) {
            if(!vis[i]&&d[v][i]<minc[i]) {
                pre[i]=v;
                minc[i]=d[v][i];
            }
        }
    }
}
void dfs(int u,int f,LL dis) {
    dep[u]=dis;
    for(int i=0;i<(int)g[u].size();i++) {
        int v=g[u][i];
        if(v==f) continue;
        dfs(v,u,dis+d[u][v]);
    }
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    cin>>n;
    for(int i=1;i<=n;i++) {
        for(int j=1;j<=n;j++) {
            cin>>d[i][j];
        }
    }
    bool f=0;
    for(int i=1;i<=n;i++) {
        for(int j=1;j<=n;j++) {
            if(i!=j&&d[i][j]==0) f=1;
            if(d[i][j]!=d[j][i]) f=1;
        }
    }
    if(f) {
        puts("NO");
        return 0;
    }
    prim();
    for(int i=2;i<=n;i++) {
        g[i].push_back(pre[i]);
        g[pre[i]].push_back(i);
    }
    f=1;
    for(int i=1;i<=n&&f;i++) {
        dfs(i,0,0);
        for(int j=1;j<=n;j++) {
            if(dep[j]!=d[i][j]) f=0;
        }
    }
    printf("%s\n",f? "YES":"NO");
    return 0;
}
