import java.util.*;
import java.io.*;
public class cf469B {
    final static int N=1005;
    static boolean vis[]=new boolean[N];
    static int a[]=new int[N];
    static int b[]=new int[N];
    static boolean cheak(int t,int n) {
        for(int i=0;i<n;i++) {
            for(int j=a[i]+t;j<=b[i]+t&&j<N;j++) {
                if(vis[j]) return true;
            }
        }
        return false;
    }
    public static void main(String args[]) throws Exception {
        //System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        int p=in.nextInt();
        int q=in.nextInt();
        int l=in.nextInt();
        int r=in.nextInt();
        Arrays.fill(vis,false);
        for(int i=0;i<p;i++) {
            int x=in.nextInt();
            int y=in.nextInt();
            for(int j=x;j<=y;j++) {
                vis[j]=true;
            }
        }
        for(int i=0;i<q;i++) {
            a[i]=in.nextInt();
            b[i]=in.nextInt();
        }
        int cont=0;
        for(int i=l;i<=r;i++) {
            if(cheak(i,q)) {
                cont++;
            }
        }
        System.out.println(cont);
    }
}
