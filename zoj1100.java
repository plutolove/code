import java.util.*;
import java.io.*;
import java.math.*;
public class zoj1100 {
    public static void main(String args[]) throws Exception{
        Mondriaans_Dream tmp=new Mondriaans_Dream();
        tmp.solve();
    }
}
class Mondriaans_Dream {
    final static int N=1<<12;
    int n,m;
    long dp[][]=new long[2][N];
    long add;
    void dfs(int r,int st,int c) {
        if(c==m) {
            dp[r][st]+=add;
            return;
        }
        dfs(r,st,c+1);
        if(c<m-1&&(st&1<<c)==0&&(st&1<<(c+1))==0) {
            dfs(r,st|(1<<c)|(1<<(c+1)),c+2);
        }
    }
    void solve() throws Exception{
        System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        while(true) {
            n=in.nextInt();
            m=in.nextInt();
            if(n==0&&m==0) break;
            int s=(1<<m);
            Arrays.fill(dp[0],0);
            Arrays.fill(dp[1],0);
            add=1;
            dfs(0,0,0);
            for(int i=1;i<n;i++) {
                Arrays.fill(dp[i%2],0);
                for(int j=0;j<s;j++) {
                    if(dp[(i-1)%2][j]==0) continue;
                    add=dp[(i-1)%2][j];
                    dfs(i%2,~j&(s-1),0);
                }
            }
            System.out.println(dp[(n-1)%2][s-1]);
        }
    }
}
