#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
using namespace std;
const int N=1000005;
bool isp[N];
vector<int> num;
void init() {
    memset(isp,0,sizeof(isp));
    for(int i=2;i<N;i++) {
        if(isp[i]) {
            num.push_back(i);
            continue;
        }
        for(int j=i*2;j<N;j+=i) {
            isp[j]=1;
        }
    }
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    init();
    int n;
    while(cin>>n) {
        for(int i=0;i<(int)num.size();i++) {
            int tmp=n-num[i];
            if(isp[tmp]) {
                cout<<num[i]<<"  "<<tmp<<endl;
                break;
            }
        }
    }
    return 0;
}
