#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
const int N=5;
const int inf=1e9;

class Coordinate
{
public:
    double xCoordinate;
    double yCoordinate;

    Coordinate(double x = 0,double y = 0)
    {
        this->xCoordinate = x;
        this->yCoordinate = y;
    }

    bool operator!=(Coordinate const &comp) const
    {
        return (this->xCoordinate != comp.xCoordinate ||
                this->yCoordinate != comp.yCoordinate);
    }
};

bool Judge(Coordinate const x,Coordinate const y,Coordinate const z)
{
    Coordinate *mVector = new Coordinate(x.xCoordinate - y.xCoordinate, x.yCoordinate - y.yCoordinate);
    Coordinate *nVector = new Coordinate(z.xCoordinate -(x.xCoordinate + y.xCoordinate)/2,z.yCoordinate-(x.yCoordinate + y.yCoordinate)/2);
    bool result = ((mVector->xCoordinate * nVector->xCoordinate +
                    mVector->yCoordinate * nVector->yCoordinate) == 0);

    if(result)
        result = (mVector->xCoordinate * mVector->xCoordinate +
                                mVector->yCoordinate * mVector->yCoordinate)
                 == ((nVector->xCoordinate * nVector->xCoordinate +
                                        nVector->yCoordinate * nVector->yCoordinate) * 4);

    delete mVector;
    delete nVector;

    return result;
}

bool IsSquare(Coordinate *array,int length)
{
    if(length != 4)
        return false;
    int a,b,c;

    if(Judge(array[0],array[1],array[2]))
    {
        a = 0;
        b = 1;
        c = 2;
    }
    else if(Judge(array[0],array[2],array[1]))
    {
        a = 0;
        b = 2;
        c = 1;
    }
    else if(Judge(array[2],array[1],array[0]))
    {
        a = 1;
        b = 2;
        c = 0;
    }
    else
        return false;

    return (array[3] != array[c] && Judge(array[a],array[b],array[3]));
}

Coordinate p[N][N],h[N];

void solve() {
    for(int i=0;i<4;i++) {
        cin>>p[i][0].xCoordinate>>p[i][0].yCoordinate;
        cin>>h[i].xCoordinate>>h[i].yCoordinate;
    }
    for(int i=0;i<4;i++) {
        for(int j=1;j<4;j++) {
            p[i][j].xCoordinate=-(p[i][j-1].yCoordinate-h[i].yCoordinate)+h[i].xCoordinate;
            p[i][j].yCoordinate=(p[i][j-1].xCoordinate-h[i].xCoordinate)+h[i].yCoordinate;
        }
    }
    int ans=inf;
    for(int i=0;i<4;i++) {
        for(int j=0;j<4;j++) {
            for(int k=0;k<4;k++) {
                for(int t=0;t<4;t++) {
                    Coordinate tmp[4];
                    tmp[0]=p[0][i];
                    tmp[1]=p[1][j];
                    tmp[2]=p[2][k];
                    tmp[3]=p[3][t];
                    if(IsSquare(tmp,4)) {
                        ans=min(ans,i+j+k+t);
                    }
                }
            }
        }
    }
    if(ans>=inf) ans=-1;
    cout<<ans<<endl;
}
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n;
    cin>>n;
    while(n--) {
        solve();
    }
    return 0;
}
