import java.util.*;
import java.io.*;
import java.math.*;
public class CF474B {
    final static int N=100005;
    static int a[]=new int[N];
    public static void main(String args[]) {
        InputReader in=new InputReader(System.in);
        PrintWriter out=new PrintWriter(System.out);
        int n=in.nextInt();
        for(int i=0;i<n;i++) {
            a[i]=in.nextInt();
        }
        for(int i=1;i<n;i++) {
            a[i]+=a[i-1];
        }
        int m=in.nextInt();
        for(int i=0;i<m;i++) {
            int q=in.nextInt();
            int res=Arrays.binarySearch(a,0,n-1,q);
            if(res<0) res=-res;
            else res++;
            out.println(res);
        }
        out.close();
    }
}
class InputReader {
    public BufferedReader reader;
    public StringTokenizer tokenizer;

    public InputReader(InputStream stream) {
        reader=new BufferedReader(new InputStreamReader(stream));
        tokenizer=null;
    }

    public String next() {
        while(tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(reader.readLine());
            }catch(IOException e) {
                throw new RuntimeException(e);
            }
        }
        return tokenizer.nextToken();
    }

    public int nextInt() {
        return Integer.parseInt(next());
    }

    public double nextDouble() {
        return Double.parseDouble(next());
    }

    public long nextLong() {
        return Long.parseLong(next());
    }

    public BigInteger nextBigInteger() {
        return new BigInteger(next());
    }
}
