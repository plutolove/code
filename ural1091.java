import java.io.*;
import java.util.*;
public class ural1091 {
    public static void main(String args[]) throws Exception{
        SOLVE ural=new SOLVE();
        ural.init();
        ural.solve();
    }
}
class SOLVE {
    static final int N=55;
    static final int p[]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47};
    int n,m;
    long c[][]=new long[N][N];
    void init() {
        for(int i=0;i<N;i++) {
            Arrays.fill(c[i],0);
        }
        c[1][0]=1;
        c[1][1]=1;
        for(int i=2;i<N;i++) {
            c[i][0]=1;
            for(int j=1;j<=i;j++) {
                c[i][j]=c[i-1][j]+c[i-1][j-1];
            }
        }
    }
    void solve() throws Exception{
        //System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        n=in.nextInt();
        m=in.nextInt();
        int len=p.length;
        long res=0;
        for(int u:p) {
            int tmp=m/u;
            res+=c[tmp][n];
        }
        for(int i=0;i<len;i++) {
            for(int j=i+1;j<len;j++) {
                int tmp=m/p[i]/p[j];
                res-=c[tmp][n];
            }
        }
        for(int i=0;i<len;i++) {
            for(int j=i+1;j<len;j++) {
                for(int k=j+1;k<len;k++) {
                    int tmp=m/p[i]/p[j]/p[k];
                    res+=c[tmp][n];
                }
            }
        }
        if(res>10000) res=10000;
        System.out.println(res);
    }
}
