import java.util.*;
import java.io.*;
import java.math.*;
public class CF474A {
    public static void main(String args[]) {
        InputReader in=new InputReader(System.in);
        PrintWriter out=new PrintWriter(System.out);
        String str="qwertyuiopasdfghjkl;zxcvbnm,./";
        char ch=in.next().charAt(0);
        String s=in.next();
        for(char i=0;i<s.length();i++) {
            out.print(str.charAt(str.indexOf(s.charAt(i))+(ch=='R'? -1:1)));
        }
        out.println();
        out.close();
    }
}
class InputReader {
    public BufferedReader reader;
    public StringTokenizer tokenizer;

    public InputReader(InputStream stream) {
        reader=new BufferedReader(new InputStreamReader(stream));
        tokenizer=null;
    }

    public String next() {
        while(tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(reader.readLine());
            }catch(IOException e) {
                throw new RuntimeException(e);
            }
        }
        return tokenizer.nextToken();
    }

    public int nextInt() {
        return Integer.parseInt(next());
    }

    public double nextDouble() {
        return Double.parseDouble(next());
    }

    public long nextLong() {
        return Long.parseLong(next());
    }

    public BigInteger nextBigInteger() {
        return new BigInteger(next());
    }
}
