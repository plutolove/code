#include<iostream>
#include<cstring>
#include<cstdio>
#include<string>
using namespace std;
const int N=1005;
const int M=15;
double p[M][N];
double dp[N][(1<<11)-1];
void init() {
    for(int i=0;i<N;i++) {
        for(int j=0;j<=(1<<10);j++) {
            dp[i][j]=-99999;
        }
    }
}
int main() {
#ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
#endif
    int t;
    scanf("%d",&t);
    for(int kc=1;kc<=t;kc++) {
        int n,m;
        scanf("%d%d",&n,&m);
        for(int i=1;i<=n;i++) {
            for(int j=1;j<=m;j++) {
                scanf("%lf",&p[i][j]);
            }
        }
        init();
        dp[0][0]=0;
        int all=(1<<n)-1;
        for(int i=1;i<=m;i++) {
            for(int s=0;s<(1<<n);s++) {
                int now=s;
                if(now==all) now=0;
                if(dp[i-1][s]<0) continue;
                for(int k=0;k<n;k++) {
                    int next=(1<<k);
                    if(now&next) continue;
                    dp[i][now|next]=max(dp[i][now|next],dp[i-1][s]+p[k+1][i]);
                }
            }
        }
        double ans=-1;
        for(int i=0;i<=all;i++) {
            ans=max(ans,dp[m][i]);
        }
        printf("Case #%d: %.5lf\n",kc,ans);
    }
    return 0;
}
