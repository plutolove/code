import java.util.*;
import java.io.*;
import java.math.*;
public class ural1045 {
    public static void main(String args[]) throws Exception{
        Solve tmp=new Solve();
        tmp.solve();
    }
}
class Solve {
    final int N=1005;
    ArrayList<ArrayList<Integer>> g;
    int vis[]=new int[N];//表示一个人走到当前点是必胜态还是必败态
    int n,k;
    int id;
    int dfs(int f,int u) {
        if(vis[u]!=-1) return vis[u];
        int res=0;
        ArrayList<Integer> tmp=g.get(u);
        boolean flag=false;
        for(int v:tmp) {
            if(v==f) continue;
            int hehe=dfs(u,v);
            if(hehe==0) {
                flag=true;
                if(u==k) {
                    id=Math.min(id,v);
                }
            }
        }
        if(flag) res=1;
        vis[u]=res;
        return res;
    }
    void solve() throws Exception{
        //System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        n=in.nextInt();
        k=in.nextInt();
        g=new ArrayList<ArrayList<Integer>>();
        for(int i=0;i<=n;i++) {
            g.add(new ArrayList<Integer>());
        }

        int u,v;
        id=9999999;
        ArrayList<Integer> tmp;
        for(int i=1;i<n;i++) {
            u=in.nextInt();
            v=in.nextInt();
            tmp=g.get(u);
            tmp.add(v);

            tmp=g.get(v);
            tmp.add(u);
        }
        Arrays.fill(vis,-1);
        int ans=dfs(-1,k);
        if(vis[k]==1) System.out.println("First player wins flying to airport "+id);
        else System.out.println("First player loses");
    }
}
