import java.util.*;
import java.io.*;
public class cf463E {
    public static void main(String args[]) throws Exception{
        SOLOVE tmp=new SOLOVE();
        tmp.solve();
    }
}
class CF463E {
    final static int N=100005;
    static ArrayList<Integer>[] g;
    int f[]=new int[N];
    int val[]=new int[N];
    int gcd(int a,int b) {
        if(b==0) return a;
        return gcd(b,a%b);
    }
    void dfs(int p,int  u) {
        f[u]=p;
        for(int v:g[u]) {
            if(v==p) continue;
            dfs(u,v);
        }
    }
    void solve() throws Exception{
        System.setIn(new BufferedInputStream(new FileInputStream("in.txt")));
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int q=in.nextInt();
        g=new ArrayList[N];
        Arrays.fill(f,0);
        g[0]=new ArrayList<>();
        for(int i=1;i<=n;i++) {
            val[i]=in.nextInt();
            g[i]=new ArrayList<>();
        }
        for(int i=1;i<n;i++) {
            int u=in.nextInt();
            int v=in.nextInt();
            g[u].add(v);
            g[v].add(u);
        }
        dfs(0,1);
        for(int i=0;i<q;i++) {
            int x=in.nextInt();
            if(x==1) {
                int y=in.nextInt();
                int res=-1;
                for(int j=f[y];j!=0;j=f[j]) {
                    if(gcd(val[j],val[y])>=2) {
                        res=j;
                        break;
                    }
                }
                System.out.println(res);
            }else {
                int y=in.nextInt();
                int z=in.nextInt();
                val[y]=z;
            }
        }
    }
}
