#include<iostream>
#include<cstring>
#include<cstdio>
#include<algorithm>
using namespace std;
const int N=200005;
int x[N];
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int tt;
    scanf("%d",&tt);
    for(int kc=1;kc<=tt;kc++) {
        int n,m,l;
        scanf("%d%d%d",&n,&m,&l);
        for(int i=0;i<n;i++) {
            scanf("%d",&x[i]);
        }
        sort(x,x+n);

        x[n]=m;
        int now=0;//表示当前的位置
        int pre=-l;//表示上一步的位置
        int res=0;//结果

        for(int i=0;i<=n;i++) {
            int next=x[i];
            int dis=next-now;
            int t=dis/(l+1);
            pre+=t*(l+1);
            res+=t*2;
            if(next-pre>l) {//当上一步不可以走到 next 位置那么就要多走一步而且pre=pre=now+t*(l+1)
                pre=now+t*(l+1);
                now=next;
                res++;
            }else if(next-pre<=l) {//当上一步能走到 next 那么直接走就可以了
                now=next;
            }
        }
        printf("Case #%d: %d\n",kc,res);
    }
    return 0;
}
