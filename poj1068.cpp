#include<iostream>
#include<cstring>
#include<cstdio>
#include<stack>
using namespace std;
const int N=10000;
int a[N],b[N];
int sum[N];
int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    int n;
    a[0]=0;
    int t;
    cin>>t;
    while(t--) {
        cin>>n;
        for(int i=1;i<=n;i++) {
            cin>>a[i];
        }
        string str="";
        for(int i=1;i<=n;i++) {
            int cont=a[i]-a[i-1];
            while(cont--) {
                str+='(';
            }
            str+=')';
        }
        sum[0]=0;
        if(str[0]==')') sum[0]=1;
        for(int i=0;i<str.length();i++) {
            sum[i+1]=sum[i];
            if(str[i]==')') sum[i+1]++;
        }
        stack<int> s;
        for(int i=0;i<str.length();i++) {
            if(str[i]=='(') s.push(i);
            else {
                int r=i+1;
                int l=s.top()+1;
                s.pop();
                cout<<sum[r]-sum[l-1]<<" ";
            }
        }
        cout<<endl;
    }
    return 0;
}
